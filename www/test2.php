<html>
<head>
<title>Получение, проверка и обработка данных на PHP</title>
</head>
<body>

<?php

$t="";
$nic="";
$num="";

$error=false;
$on_submit=false;
$pol="";

$pol_m="";
$pol_j="";
$pol_x="";
$mas_pol=array ("m"=>"мужской","j"=>"женский","x"=>"еще не поняло");
$selected=" selected='selected' ";

$coment="";

if (!empty($_POST["onsubmit"]))
{
//форма была отправлена, проверим данные
$on_submit=true;
if (empty($_POST["nic"]))
{
$t.="<div>Вы не указали свое имя или ник!</div>";
$error=true;
}
else
$nic=$_POST["nic"];
if (empty($_POST["number"]))
{
$t.="<div>Вы не указали свой возраст!</div>";
$error=true;
}
else
{
$num=$_POST["number"];
if (!is_numeric($num))
{
$t.="<div>Возраст нужно написать цифрами!</div>";
$error=true;
}

}
if (empty($_POST["pol"]) || $_POST["pol"]=="none")
{
$t.="<div>Укажите свой пол!</div>";
$error=true;
}
else
{
$pol=$_POST["pol"];
switch ($pol)
{
case ("m"):$pol_m=$selected;break;
case ("j"):$pol_j=$selected;break;
case ("x"):$pol_x=$selected;break;
}
}
if (empty($_POST["coment"]))
{
$t.="<div>Вы не написали комментарий!</div>";
$error=true;
}
else
$coment=$_POST["coment"];
}

if ($on_submit && !$error)
{
//если форма была отправлена и ошибок при заполнении нет, то отображаем поученные данные
$t.="Получены следующие данные<br/>Ваше имя: ".htmlspecialchars($nic,ENT_QUOTES);
$t.="<br/>Ваш возраст: ".htmlspecialchars($num,ENT_QUOTES);
$t.="<br/>Ваш пол: ".$mas_pol[$pol];
$t.="<br/>Комментарий: ".htmlspecialchars($coment,ENT_QUOTES);
}
else
{
//если форма не отправлялась или была отправлена, но при заполнении полей были допущены ошибки, то выводим форму
//причем, если форма была отправлена (с некорректным заполнением полей), то выводим ее с теми данными, которые ввел пользователь
$t.="<form action='text.php' method='post'>

Ваше имя или ник<br/>
<input type='text' name='nic' value='".htmlspecialchars($nic,ENT_QUOTES)."'><br/>
Сколько вам лет (цифрами)<br/>
<input type='text' name='number' value='".htmlspecialchars($num,ENT_QUOTES)."'>
<br/>
Ваш пол<br/>
<select name='pol'>
<option value='none'>выберите...</option>
<option value='m' ".$pol_m.">мужской</option>
<option value='j' ".$pol_j.">женский</option>
<option value='x' ".$pol_x.">еще не поняло</option>
</select>
<br/>
Напишите комментарий<br/>
<textarea name='coment'>".htmlspecialchars($coment,ENT_QUOTES)."</textarea>
<br/>
<input type='submit' value='Отправить'>
<input type='hidden' name='onsubmit' value='on'>
</form>
";
}
echo ($t);

?>

</body>
</html>